#include <stdlib.h>
#include <time.h>
#include <ncurses.h>
#include "config.h"


#if STUDY_SESSION_25
#define TIMER_S 25
#endif


#if STUDY_SESSION_50
#define TIMER_S 50
#endif


#if BREAK_TIME_5
#define TIMER_B 5
#endif


#if BREAK_TIME_10
#define TIMER_B 10
#endif


#define STUDY 0
#define BREAK 1

char help_msg[] = "\n-s integer\t\tsets length of studying session, in minutes\n"
                    "-b integer\t\tsets length of breaks, in minutes\n"
                    "-r integer\t\tnumber of sessions\n"
                    "-h        \t\tprints help message lol \n\n"; 

long n_repeat = 4, time_elapsed = 0, timer_s = TIMER_S, timer_b = TIMER_B, check = 0;
long time_started = 0;


void start_timer(int, int);
void countdown(long, short, char*, int);
void fact_check(char*, long int*);


int main(int argc, char *argv[]){
  
  if( argv[1] != NULL ){

    for(int i = 1; i < argc ; i++){

      switch ( *(argv[i]+1) ){

        case 'h':
          printf("%s", help_msg);
          exit(0);
          break;

        case 's':
          fact_check(argv[i+1], &timer_s);
          break;

        case 'b':
          fact_check(argv[i+1], &timer_b);
          break;

        case 'r':
          fact_check(argv[i+1], &n_repeat);
          break;

      }
    }
  }

   
  initscr();
  noecho();

  for(int i = 1; i <= n_repeat; i++){
    
    
    start_timer(STUDY, i);
    system("notify-send 'POMODORO' 'IT IS TIME FOR A BREAK' ");
    start_timer(BREAK, i);
    
    
  }

  endwin();
  return 0;
}







void start_timer(int x, int i){
  
  time_started  = time(0);

  if(x == STUDY)
    countdown(time_started, timer_s, "STUDY SESSION", i);
     
  if(x == BREAK)
    countdown(time_started, timer_b, "BREAK", i);
  

}


void countdown(long time_started, short timer, char *type, int i){

  struct timespec tim;
    tim.tv_sec  = 0;
    tim.tv_nsec = 333000000;

  int count_timer = 1, x, y;
  short mins = 0, secs = 0;


  while(count_timer > 0){
       
      nanosleep(&tim, NULL);
      short old_secs = secs;
      short ch;

     
      time_elapsed = time(0) - time_started;
    
      count_timer = (timer * 60) - time_elapsed;

      mins = count_timer / 60;
      secs = count_timer - mins * 60;


      if(old_secs != secs){
        printw("Session %d of %d\n\n", i, n_repeat);
        printw("%s => %d:%02d", type, mins, secs);
        refresh();
        
        clear();
        move(0, 0);
      
        }


    }
}

void fact_check(char *arg, long int *result) {
  *result = strtol(arg, NULL, 10);
}




